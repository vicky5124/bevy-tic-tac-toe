use bevy::prelude::*;

type TripleIndex = (usize, usize, usize);

static WIN_CONDITIONS: [TripleIndex; 8] = [
    (0, 1, 2),
    (3, 4, 5),
    (6, 7, 8),
    (0, 3, 6),
    (1, 4, 7),
    (2, 5, 8),
    (0, 4, 8),
    (6, 4, 2),
];

#[derive(Debug, Clone, Copy, PartialEq)]
enum Piece {
    Cross,
    Circle,
    None,
}

#[derive(Debug, Clone)]
struct CurrentPiece(Piece);

#[derive(Debug, Clone)]
struct Board {
    pieces: [Piece; 9],
    winner: (Piece, TripleIndex),
}

#[derive(Debug, Clone)]
struct Position(usize);

#[derive(Default)]
struct State {
    cursor: EventReader<CursorMoved>,
}

impl Default for Piece {
    fn default() -> Piece {
        Piece::None
    }
}

impl Default for Board {
    fn default() -> Board {
        Board {
            pieces: [Piece::default(); 9],
            winner: (Piece::default(), (0, 0, 0)),
        }
    }
}

impl Piece {
    fn switch(&mut self) {
        match self {
            Self::Cross => *self = Self::Circle,
            Self::Circle => *self = Self::Cross,
            _ => (),
        }
    }
}

fn main() {
    App::build()
        .add_plugins(DefaultPlugins)
        .add_plugin(bevy_webgl2::WebGL2Plugin)
        .add_resource(Board::default())
        .add_resource(CurrentPiece(Piece::Cross))
        .add_startup_system(setup.system())
        .add_system(place_piece_mouse_system.system())
        .add_system(place_piece_numbers_system.system())
        .add_system(check_win_condition_system.system())
        .add_system(display_win_condition_system.system())
        .run();
}

fn setup(
    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
) {
    // Add the game's entities to our world
    commands
        // cameras, so we can see the stuff
        .spawn(Camera2dBundle::default())
        .spawn(CameraUiBundle::default())
        // The game name
        .spawn(TextBundle {
            text: Text {
                font: asset_server.load("fonts/Hack-Regular.ttf"),
                value: "Tic\n Tac\n  Toe".to_string(),
                style: TextStyle {
                    color: Color::rgb(0.7, 0.7, 0.9),
                    font_size: 38.0,
                    ..Default::default()
                },
            },
            style: Style {
                position_type: PositionType::Absolute,
                position: Rect {
                    top: Val::Px(5.0),
                    left: Val::Px(5.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            ..Default::default()
        });

    // Add visual bounds
    let wall_material = materials.add(Color::rgb(0.7, 0.7, 0.7).into());
    let wall_thickness = 5.0;
    let bound = 600.0;

    commands
        // left
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(-bound / 2.0, 0.0, 0.0)),
            sprite: Sprite::new(Vec2::new(wall_thickness, bound + wall_thickness)),
            ..Default::default()
        })
        // right
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(bound / 2.0, 0.0, 0.0)),
            sprite: Sprite::new(Vec2::new(wall_thickness, bound + wall_thickness)),
            ..Default::default()
        })
        // bottom
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(0.0, -bound / 2.0, 0.0)),
            sprite: Sprite::new(Vec2::new(bound + wall_thickness, wall_thickness)),
            ..Default::default()
        })
        // top
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(0.0, bound / 2.0, 0.0)),
            sprite: Sprite::new(Vec2::new(bound + wall_thickness, wall_thickness)),
            ..Default::default()
        })
        // middle vertical
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(
                (-bound + (bound * 2.0 / 3.0)) / 2.0,
                0.0,
                0.0,
            )),
            sprite: Sprite::new(Vec2::new(wall_thickness, bound + wall_thickness)),
            ..Default::default()
        })
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(
                (-bound + (bound * 2.0 / 3.0 * 2.0)) / 2.0,
                0.0,
                0.0,
            )),
            sprite: Sprite::new(Vec2::new(wall_thickness, bound + wall_thickness)),
            ..Default::default()
        })
        // middle horizontal
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(
                0.0,
                (-bound + (bound * 2.0 / 3.0)) / 2.0,
                0.0,
            )),
            sprite: Sprite::new(Vec2::new(bound + wall_thickness, wall_thickness)),
            ..Default::default()
        })
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(
                0.0,
                (-bound + (bound * 2.0 / 3.0 * 2.0)) / 2.0,
                0.0,
            )),
            sprite: Sprite::new(Vec2::new(bound + wall_thickness, wall_thickness)),
            ..Default::default()
        });
}

fn display_win_condition_system(
    time: Res<Time>,
    board: Res<Board>,
    mut query: Query<(&Piece, &Position, &mut Transform)>,
) {
    if board.winner.0 != Piece::None {
        let delta_seconds = f32::min(0.2, time.delta_seconds());

        for (_piece, pos, mut transform) in query.iter_mut() {
            if pos.0 == board.winner.1 .0
                || pos.0 == board.winner.1 .1
                || pos.0 == board.winner.1 .2
            {
                // rotate the winning pieces clockwise
                transform.rotate(Quat::from_rotation_z(delta_seconds));
            }
        }
    }
}
fn check_win_condition_system(
    mut board: ResMut<Board>,
) {
    for i in &WIN_CONDITIONS {
        if board.pieces[i.0] == Piece::Cross
            && board.pieces[i.1] == Piece::Cross
            && board.pieces[i.2] == Piece::Cross
        {
            board.winner = (Piece::Cross, *i);
        }
        if board.pieces[i.0] == Piece::Circle
            && board.pieces[i.1] == Piece::Circle
            && board.pieces[i.2] == Piece::Circle
        {
            board.winner = (Piece::Circle, *i);
        }
    }
}

fn place_piece_mouse_system(
    mut state: Local<State>,
    mouse_button_input: Res<Input<MouseButton>>,
    mouse_motion: Res<Events<CursorMoved>>,

    mut board: ResMut<Board>,
    mut current_piece: ResMut<CurrentPiece>,

    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
) {
    if board.winner.0 != Piece::None {
        return;
    }

    let mut translation = None;
    let mut position = None;

    if mouse_button_input.pressed(MouseButton::Left) {
        for event in state.cursor.iter(&mouse_motion) {
            let x = event.position.x;
            let y = event.position.y;

            info!("x: {} | y: {}", x, y);

            if (x < 540.0 && x > 340.0) && (y < 660.0 && y > 460.0) {
                translation = Some(Vec3::new(-200.0, 200.0, 0.0));
                position = Some(0);
            } else if (x < 740.0 && x > 540.0) && (y < 660.0 && y > 460.0) {
                translation = Some(Vec3::new(0.0, 200.0, 0.0));
                position = Some(1);
            } else if (x < 940.0 && x > 740.0) && (y < 660.0 && y > 460.0) {
                translation = Some(Vec3::new(200.0, 200.0, 0.0));
                position = Some(2);
            } else if (x < 540.0 && x > 340.0) && (y < 460.0 && y > 260.0) {
                translation = Some(Vec3::new(-200.0, 0.0, 0.0));
                position = Some(3);
            } else if (x < 740.0 && x > 540.0) && (y < 460.0 && y > 260.0) {
                translation = Some(Vec3::new(0.0, 0.0, 0.0));
                position = Some(4);
            } else if (x < 940.0 && x > 740.0) && (y < 460.0 && y > 260.0) {
                translation = Some(Vec3::new(200.0, 0.0, 0.0));
                position = Some(5);
            } else if (x < 540.0 && x > 340.0) && (y < 260.0 && y > 60.0) {
                translation = Some(Vec3::new(-200.0, -200.0, 0.0));
                position = Some(6);
            } else if (x < 740.0 && x > 540.0) && (y < 260.0 && y > 60.0) {
                translation = Some(Vec3::new(0.0, -200.0, 0.0));
                position = Some(7);
            } else if (x < 940.0 && x > 740.0) && (y < 260.0 && y > 60.0) {
                translation = Some(Vec3::new(200.0, -200.0, 0.0));
                position = Some(8);
            }
        }
    }


    if let Some(p) = position {
        if Piece::None == board.pieces[p] {
            if let Some(t) = translation {
                let cross_texture_handle = asset_server.load("textures/cross.png");
                let circle_texture_handle = asset_server.load("textures/circle.png");

                commands
                    .spawn(SpriteBundle {
                        material: materials.add({
                            if let Piece::Cross = current_piece.0 {
                                cross_texture_handle.into()
                            } else {
                                circle_texture_handle.into()
                            }
                        }),
                        transform: Transform {
                            translation: t,
                            ..Default::default()
                        },
                        ..Default::default()
                    })
                    .with(Position(p))
                    .with(current_piece.0);

                board.pieces[p] = current_piece.0;
                current_piece.0.switch();
            }
        }
    }
}

fn place_piece_numbers_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut board: ResMut<Board>,
    mut current_piece: ResMut<CurrentPiece>,

    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
) {
    if board.winner.0 != Piece::None {
        return;
    }

    let mut translation = None;
    let mut position = None;

    if keyboard_input.just_pressed(KeyCode::Key1) {
        translation = Some(Vec3::new(-200.0, 200.0, 0.0));
        position = Some(0);
    } else if keyboard_input.just_pressed(KeyCode::Key2) {
        translation = Some(Vec3::new(0.0, 200.0, 0.0));
        position = Some(1);
    } else if keyboard_input.just_pressed(KeyCode::Key3) {
        translation = Some(Vec3::new(200.0, 200.0, 0.0));
        position = Some(2);
    } else if keyboard_input.just_pressed(KeyCode::Key4) {
        translation = Some(Vec3::new(-200.0, 0.0, 0.0));
        position = Some(3);
    } else if keyboard_input.just_pressed(KeyCode::Key5) {
        translation = Some(Vec3::new(0.0, 0.0, 0.0));
        position = Some(4);
    } else if keyboard_input.just_pressed(KeyCode::Key6) {
        translation = Some(Vec3::new(200.0, 0.0, 0.0));
        position = Some(5);
    } else if keyboard_input.just_pressed(KeyCode::Key7) {
        translation = Some(Vec3::new(-200.0, -200.0, 0.0));
        position = Some(6);
    } else if keyboard_input.just_pressed(KeyCode::Key8) {
        translation = Some(Vec3::new(0.0, -200.0, 0.0));
        position = Some(7);
    } else if keyboard_input.just_pressed(KeyCode::Key9) {
        translation = Some(Vec3::new(200.0, -200.0, 0.0));
        position = Some(8);
    }

    if let Some(p) = position {
        if Piece::None == board.pieces[p] {
            if let Some(t) = translation {
                let cross_texture_handle = asset_server.load("textures/cross.png");
                let circle_texture_handle = asset_server.load("textures/circle.png");

                commands
                    .spawn(SpriteBundle {
                        material: materials.add({
                            if let Piece::Cross = current_piece.0 {
                                cross_texture_handle.into()
                            } else {
                                circle_texture_handle.into()
                            }
                        }),
                        transform: Transform {
                            translation: t,
                            ..Default::default()
                        },
                        ..Default::default()
                    })
                    .with(Position(p))
                    .with(current_piece.0);

                board.pieces[p] = current_piece.0;
                current_piece.0.switch();
            }
        }
    }
}

//commands
//    // 1
//    .spawn(SpriteBundle {
//        material: materials.add(cross_texture_handle.clone().into()),
//        transform: Transform {
//            translation: Vec3::new(-200.0, 200.0, 0.0),
//            ..Default::default()
//        },
//        ..Default::default()
//    })
//    // 2
//    .spawn(SpriteBundle {
//        material: materials.add(cross_texture_handle.clone().into()),
//        transform: Transform {
//            translation: Vec3::new(0.0, 200.0, 0.0),
//            ..Default::default()
//        },
//        ..Default::default()
//    })
//    // 3
//    .spawn(SpriteBundle {
//        material: materials.add(cross_texture_handle.clone().into()),
//        transform: Transform {
//            translation: Vec3::new(200.0, 200.0, 0.0),
//            ..Default::default()
//        },
//        ..Default::default()
//    })
//    // 4
//    .spawn(SpriteBundle {
//        material: materials.add(cross_texture_handle.clone().into()),
//        transform: Transform {
//            translation: Vec3::new(-200.0, 0.0, 0.0),
//            ..Default::default()
//        },
//        ..Default::default()
//    })
//    // 5
//    .spawn(SpriteBundle {
//        //material: materials.add(circle_texture_handle.clone().into()),
//        material: materials.add(cross_texture_handle.clone().into()),
//        transform: Transform {
//            translation: Vec3::new(0.0, 0.0, 0.0),
//            ..Default::default()
//        },
//        ..Default::default()
//    })
//    // 6
//    .spawn(SpriteBundle {
//        material: materials.add(cross_texture_handle.clone().into()),
//        transform: Transform {
//            translation: Vec3::new(200.0, 0.0, 0.0),
//            ..Default::default()
//        },
//        ..Default::default()
//    })
//    // 7
//    .spawn(SpriteBundle {
//        material: materials.add(cross_texture_handle.clone().into()),
//        transform: Transform {
//            translation: Vec3::new(-200.0, -200.0, 0.0),
//            ..Default::default()
//        },
//        ..Default::default()
//    })
//    // 8
//    .spawn(SpriteBundle {
//        material: materials.add(cross_texture_handle.clone().into()),
//        transform: Transform {
//            translation: Vec3::new(0.0, -200.0, 0.0),
//            ..Default::default()
//        },
//        ..Default::default()
//    })
//    // 9
//    .spawn(SpriteBundle {
//        material: materials.add(cross_texture_handle.clone().into()),
//        transform: Transform {
//            translation: Vec3::new(200.0, -200.0, 0.0),
//            ..Default::default()
//        },
//        ..Default::default()
//    });
